# Virtual Labs
# A virtual lab prototype for Power electronics Full Wave Rectifier SCR created using 
* JsPlumb Toolkit for drag-n-drop and flowchart connectors functionality.
* Psim to run simulations for the required circuit components and identify their graphs.
* Plotly.js for creating graphs.
# Functionalities
* Gives wrong connection message as soon as we make a wrong connection and the color of the connector turns to red.
* On right-click a message box appears where we can set the name or value of that component.
* After setting the values when we click on add graphs for that circuit appears just below the waveform section.
* The connectors can be deleted when clicked.
* The whole webpage can also be printed by choosing the **Print** button.





